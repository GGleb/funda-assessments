﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Described connection to funda API
    /// </summary>
    public class FundaProxy : IDisposable
    {
        private const string url = @"http://partnerapi.funda.nl/feeds/Aanbod.svc/json/{0}/?type=koop&zo={1}&page={2}&pagesize=25";
        private const string key = "ac1b0b1572524640a0ecc54de453ea9f";

        private const string filterAmsterdamWithTuin = @"/amsterdam/tuin/";
        private const string filterAmsterdam = @"/amsterdam/";
        private int connectionLimit = 101;
        private Stopwatch startDate = new Stopwatch();

        private HttpClient client;

        public FundaProxy()
        {
            this.client = new HttpClient();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            client.Dispose();
            startDate.Stop();
        }

        /// <summary>
        /// Get requested page with filters from api.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="location"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        public async Task<T> GetPage<T>(Location location, int pageNumber)
        {
            await WaitingLimits();
            var message = await client.GetAsync(GetUrl(location, pageNumber));
            message.EnsureSuccessStatusCode();

            var content = await message.Content.ReadAsStringAsync();
            if (string.IsNullOrEmpty(content))
                throw new Exception("null response content");

            return JsonConvert.DeserializeObject<T>(content);

        }

        /// <summary>
        /// Waiting if done around 100 requests per minute
        /// </summary>
        /// <returns></returns>
        private async Task WaitingLimits()
        {
            connectionLimit--;
            startDate.Start();
            if (connectionLimit == 0 && startDate.ElapsedMilliseconds < 60000)
            {
                await Task.Delay(TimeSpan.FromMilliseconds(60000 - startDate.ElapsedMilliseconds));
                connectionLimit = 100;
                startDate.Restart();
            }
        }

        /// <summary>
        /// Build url for request to Funda API.
        /// </summary>
        /// <param name="location"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        private string GetUrl(Location location, int page)
        {
            string filter;
            switch (location)
            {
                case Location.Amsterdam:
                    filter = filterAmsterdam;
                    break;
                case Location.AmsterdamWithTuin:
                    filter = filterAmsterdamWithTuin;
                    break;
                default: throw new ArgumentOutOfRangeException("This location is not supported yet");
            }

            return string.Format(url, key, filter, page);
        }

    }
}
