﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Descriped Makelaar rating
    /// </summary>
    public class MakelaarRating
    {
        private List<Makelaar> ratingList = new List<Makelaar>();

        /// <summary>
        /// Add items to rating 
        /// </summary>
        /// <param name="makelaars"></param>
        public void AddToRating(IEnumerable<Makelaar> makelaars)
        {
            ratingList.AddRange(makelaars);
        }

        /// <summary>
        /// Get top 10 makelaars
        /// </summary>
        /// <returns></returns>
        public List<Makelaar> GetTop10()
        {
            return CalculateRating().OrderByDescending(x => x.Count).Take(10).ToList();
        }

        /// <summary>
        ///Calculating rating
        /// </summary>
        /// <returns></returns>
        private List<Makelaar> CalculateRating()
        {
            return ratingList.GroupBy(x => x.Id).Select(g => new Makelaar
            {
                Id = g.Key,
                Name = g.FirstOrDefault().Name,
                Count = g.Sum(s => s.Count)
            }).ToList();
        }
    }
}
