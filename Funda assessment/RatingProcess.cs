﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Rating Process
    /// </summary>
    public class RatingProcess
    {
        /// <summary>
        /// Start rating process
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public async Task<List<Makelaar>> Start(Location location)
        {
            int maxPage = 1;
            var makelaarAmsterdamRating = new MakelaarRating();
            using (var proxy = new FundaProxy())
            {
                for (int i = 0; i <= maxPage; i++)
                {
                    var root = await proxy.GetPage<FundaRoot>(location, i);
                    makelaarAmsterdamRating.AddToRating(root?.Objects?.GroupBy(x => x.MakelaarId).Select(g => new Makelaar
                    {
                        Id = g.Key,
                        Name = g.FirstOrDefault().MakelaarNaam,
                        Count = g.Count()
                    }));
                    maxPage = root?.Paging?.AantalPaginas == maxPage ? maxPage : root?.Paging?.AantalPaginas ?? 0;
                }
            }
            return makelaarAmsterdamRating.GetTop10();
        }
    }
}
