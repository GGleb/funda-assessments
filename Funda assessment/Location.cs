﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Locations for filter
    /// </summary>
    public enum Location
    {
        Amsterdam,
        AmsterdamWithTuin
    }
}
