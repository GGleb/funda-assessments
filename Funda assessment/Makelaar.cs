﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Described Makelaar
    /// </summary>
    public class Makelaar
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
