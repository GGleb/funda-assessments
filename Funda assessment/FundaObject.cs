﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Item with needed fields of collection "Objects" from input json.
    /// </summary>
    public class FundaObject
    {
        public long MakelaarId { get; set; }
        public string MakelaarNaam { get; set; }
    }
}
