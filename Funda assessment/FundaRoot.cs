﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Root type with needed fields from input json.
    /// </summary>
    public class FundaRoot
    {
        public List<FundaObject> Objects { get; set; }
        public FundaPaging Paging { get; set; }
    }
}
