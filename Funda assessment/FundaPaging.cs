﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    /// <summary>
    /// Type Paging with needed fields from input json.
    /// </summary>
    public class FundaPaging
    {
        public int AantalPaginas { get; set; }
    }
}
