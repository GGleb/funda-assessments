﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funda_assessment
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync().GetAwaiter().GetResult();
        }

        public static async Task MainAsync()
        {
            var worker = new RatingProcess();
            PrintTitle("Please wait, we are searching top 10 makelaars in Amsterdam");
            var result = await worker.Start(Location.Amsterdam);
            PrintResult("Amsterdam", result);
            PrintTitle("Please wait, we are searching top 10 makelaars in Amsterdam with tuin");
            result = await worker.Start(Location.AmsterdamWithTuin);
            PrintResult("Amsterdam with tuin", result);

            Console.ReadKey();
        }

        /// <summary>
        /// Printing result
        /// </summary>
        /// <param name="title"></param>
        /// <param name="makelaars"></param>
        private static void PrintResult(string title, List<Makelaar> makelaars)
        {
            Console.WriteLine("#  Name  Id  Objects");
            Console.WriteLine(string.Empty);
            int i = 0;
            foreach (var m in makelaars)
            {
                Console.WriteLine($"{++i}  {m.Name}  {m.Id}  {m.Count}");
            }
        }

        /// <summary>
        /// Printing title
        /// </summary>
        /// <param name="title"></param>
        private static void PrintTitle(string title)
        {
            Console.WriteLine(string.Empty);
            Console.WriteLine(title);
        }
    }
}
